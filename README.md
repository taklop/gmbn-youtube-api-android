# **GMBN-YouTube-API-Android**

A basic application to display a thumbnail for videos from the GMBN YouTube channel, with a secondary view displaying details for each video.

**YouTube Data API**

The id for a channel can be obtained from the username of a YouTube channel. The username can be found by going to a channels ABOUT section, as here: 
    https://www.youtube.com/user/globalmtb/about
The username here is globalmtb.

With the username, a request can be made to:
    https://www.googleapis.com/youtube/v3/channels?part=snippet%2CcontentDetails&forUsername={username}&key={apikey}
The content details argument makes the response include a "relatedPlaylists" field, which includes reference to a playlist ID for "uploads". This is used to find a list of all videos a channel has uploaded.

To find the list of videos in a playlist, the playlistItems listing call can be made:
    https://www.googleapis.com/youtube/v3/playlistItems?part=contentDetails%2Csnippet&maxResults=50&playlistId={playlistID}&key={apikey}

**Uploads playlist ID of gmbn : UU_A--fhX5gea0i4UtpD99Gg**

**To-Do & more**
    
In a real app I would likely abstract the above process, rather than making requests to hardcode the specific playlistID. For testing / time purposes I found the specific playlist ID I needed.

The above request returns at most 50 playlistItems. It is possible to use pagination to return the items before / after a certain request, where total number of items > max results. This could be integrated with the recycler view to dynamically request more data when a user scrolls further down or up (Only loading a max number at a time)

If I'd had the time, I would've preferred the header thumbnail image to collapse when you scroll down.

I also rushed too much and didn't add unit testing, which I'd have like to have done.
        


