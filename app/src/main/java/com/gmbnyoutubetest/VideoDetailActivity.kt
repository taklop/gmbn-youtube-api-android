package com.gmbnyoutubetest

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.video_detail.*

class VideoDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.video_detail)
        setThumbnail(intent)
        setTitle(intent)
        setDatePublished(intent)
        setDescription(intent)
    }

    fun setTitle(intent: Intent) {
        val title = intent.getStringExtra(CustomViewHolder.NAME_KEY)
        videoTitle.text = title
    }
    fun setDatePublished(intent: Intent) {
        val date = intent.getStringExtra(CustomViewHolder.DATE_PUBLISHED_KEY)
        datePublished.text = date
    }
    fun setDescription(intent: Intent) {
        val description = intent.getStringExtra(CustomViewHolder.DESCRIPTION_KEY)
        videoDescription.text = description
    }

    fun setThumbnail(intent: Intent){
        val thumbnailUrl = intent.getStringExtra(CustomViewHolder.THUMBNAIL_KEY)
        Picasso.get().load(thumbnailUrl).into(thumbnailView)
    }
}