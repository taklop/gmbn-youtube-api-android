package com.gmbnyoutubetest

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.*
import java.io.IOException
import java.lang.Exception

class MainActivity : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var recyclerAdapter: RecyclerAdapter
    private lateinit var recyclerViewManager: LinearLayoutManager

    private val baseURL: String = "https://www.googleapis.com/youtube/v3/"
    private val uploadsPlaylistId = "UU_A--fhX5gea0i4UtpD99Gg"
    //This should be stored out of the Repo, it is only here for testing purposes
    private val apiKey = "AIzaSyAXzR2-yr6YUN0R86iHxt1a_Y51BSCQDzE"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setTheme(R.style.AppTheme)
        recyclerViewManager = LinearLayoutManager(this)

        recyclerView = thumbnailRecyclerView.apply {
            layoutManager = recyclerViewManager
        }

        fetchVideoList()
    }

    fun fetchVideoList() : PlaylistItemList? {
        val endpoint = buildEndpoint()
        val request = Request.Builder().url(endpoint).build()
        val client = OkHttpClient()
        var playlistItemList: PlaylistItemList? = null

        client.newCall(request).enqueue(object : Callback {

            override fun onResponse(call: Call, response: Response) {
                val responseBody = response.body()?.string()
                val gsonBuilder = GsonBuilder().create()

                playlistItemList = gsonBuilder.fromJson(responseBody, PlaylistItemList::class.java)
                recyclerAdapter = RecyclerAdapter(playlistItemList)

                runOnUiThread {
                    progressBar.visibility = View.INVISIBLE
                    recyclerView.adapter = recyclerAdapter
                }
            }

            override fun onFailure(call: Call, e: IOException) {
                throw Exception("Error! Failed to connect to API endpoint")
            }
        })
        return playlistItemList
    }

    fun buildEndpoint(): String {
        return baseURL + "playlistItems?part=contentDetails%2Csnippet&maxResults=50&playlistId=" + uploadsPlaylistId + "&key=" + apiKey
    }
}
