package com.gmbnyoutubetest

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View

class CustomViewHolder(view: View, var videoDetails: VideoDetail) : RecyclerView.ViewHolder(view) {

    companion object {
        const val NAME_KEY = "VIDEO_NAME"
        const val THUMBNAIL_KEY = "THUMBNAIL"
        const val DESCRIPTION_KEY = "DESCRIPTION"
        const val DATE_PUBLISHED_KEY = "DATE_PUBLISHED"
    }

    init {
        view.setOnClickListener {
            var videoDetailIntent = Intent(view.context, VideoDetailActivity::class.java)
            videoDetailIntent.putExtra(NAME_KEY, videoDetails.videoName)
            videoDetailIntent.putExtra(THUMBNAIL_KEY, videoDetails.thumbnailUrl)
            videoDetailIntent.putExtra(DESCRIPTION_KEY, videoDetails.description)
            videoDetailIntent.putExtra(DATE_PUBLISHED_KEY, videoDetails.datePublished)

            view.context.startActivity(videoDetailIntent)
        }
    }

}