package com.gmbnyoutubetest

//Objects for GSON to map the YouTube Data API to

class PlaylistItemList(val items: List<Video>)

class Video(val snippet: Snippet)

class Snippet(val publishedAt: String, val title: String, val thumbnails: ThumbnailList, val description: String)

class ThumbnailList(val default: Thumbnail, val medium: Thumbnail, val high: Thumbnail, val standard: Thumbnail, val maxres: Thumbnail)

class Thumbnail(val url: String, val width: Int, val height: Int)