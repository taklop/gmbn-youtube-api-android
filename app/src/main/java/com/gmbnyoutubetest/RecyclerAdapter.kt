package com.gmbnyoutubetest

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.video_tile.view.*

class RecyclerAdapter(val playlistItemList: PlaylistItemList?): RecyclerView.Adapter<CustomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val videoTile = layoutInflater.inflate(R.layout.video_tile, parent, false)

        val videoDetail = getDetailsForPosition(position)
        return CustomViewHolder(videoTile, videoDetail)
    }

    override fun getItemCount(): Int {
        return checkNotNull(playlistItemList?.items?.count())
    }

    override fun onBindViewHolder(customHolder: CustomViewHolder, position: Int) {
        setTitle(position, customHolder)
        setThumbnail(position, customHolder)
        customHolder.videoDetails = getDetailsForPosition(position)
    }

    private fun setTitle(position: Int, customHolder: CustomViewHolder) {
        val videoName = playlistItemList?.items?.get(position)?.snippet?.title
        customHolder.itemView.videoTitleView.text = videoName
    }

    private fun setThumbnail(position: Int, customHolder: CustomViewHolder) {
        val thumbnailUrl =
            playlistItemList?.items?.get(position)?.snippet?.thumbnails?.medium?.url //medium for faster loading
        val thumbnailView = customHolder.itemView.thumbnailView

        Picasso.get().load(thumbnailUrl).into(thumbnailView)
    }

    //Returns relevant snippet for specific video position
    fun getDetailsForPosition(position: Int): VideoDetail{
        return VideoDetail(playlistItemList?.items?.get(position)?.snippet)
    }

}