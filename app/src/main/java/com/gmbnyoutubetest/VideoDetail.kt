package com.gmbnyoutubetest

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class VideoDetail(video: Snippet?) {
    var thumbnailUrl = video?.thumbnails?.maxres?.url
    var videoName = video?.title
    var datePublished = formatDateString(video?.publishedAt)
    var description = video?.description

    fun formatDateString(inputDate: String?) : String? {
        var dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US)
        var dateOutput: String
        var date: Date
        try {
            date = dateFormat.parse(inputDate)
            dateFormat = SimpleDateFormat("dd MMM yyyy 'at' HH:mm a", Locale.UK)
            dateOutput = "Published on ${dateFormat.format(date)}"
            return dateOutput
        } catch( parseException: ParseException) {
            parseException.printStackTrace()
        }
        return null
    }
}